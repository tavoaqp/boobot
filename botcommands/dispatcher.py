from botcore import *
from botcommands.movecommands import *
from botcommands.corecommands import *

class BooBotCommmandDispatcher:

	def __init__(self, bot, table):
		self.bot=bot
		self.table=table
		self.commands={}
		cmdlist=[PlaceCommand(bot,table),MoveCommand(bot,table),LeftCommand(bot,table), RightCommand(bot,table), ReportCommand(bot,table)]

		for cmdObj in cmdlist:
			self.commands[cmdObj.getName()]=cmdObj

	def dispatch(self, cmdName, args):		
		selectedCommand=self.commands[cmdName]
		selectedCommand.doCommand(args)
		if (len(selectedCommand.getMsg())>0):
			return selectedCommand.getMsg()
		else:
			return ""
