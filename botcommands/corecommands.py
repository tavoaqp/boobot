from botcore import *

class BooBotCommand:

	def __init__(self, name, bot, table):
		self.name=name
		self.bot=bot
		self.table=table
		self.msg=""	

	def getName(self):
		return self.name

	def doCommand(self,args):
		return 

	def getMsg(self):
		return self.msg

	def setMsg(self,msgValue):
		self.msg=msgValue

	def validateBotPosition(self,x_value, y_value):

		if (x_value<0 or x_value>self.table.getWidth()):
			return False

		if (y_value<0 or y_value>self.table.getHeight()):
			return False

		return True

	def setBotPosition(self,x_value, y_value, dirValue):
		if (self.validateBotPosition(x_value,y_value)):
			self.bot.setPosX(x_value)
			self.bot.setPosY(y_value)
			self.bot.setDir(dirValue)
			return True
		else:
			return False

class OnTableCommand(BooBotCommand):

	def __init__(self, name, bot, table):
		BooBotCommand.__init__(self,name,bot,table)

	def doCommand(self,args):

		if (self.bot.getStatus()!="CREATED"):
			self.doTableCommand(args)

	def doTableCommand(self, args):
		return
	

class PlaceCommand(BooBotCommand):

	def __init__(self, bot, table):
		BooBotCommand.__init__(self,"PLACE", bot, table)
		self.dirMap={}
		self.dirMap["NORTH"]=90
		self.dirMap["SOUTH"]=270
		self.dirMap["EAST"]=0
		self.dirMap["WEST"]=180

	def doCommand(self,args):
		nextX=int(args[0])
		nextY=int(args[1])

		nextDir=self.dirMap[args[2]]

		if (self.setBotPosition(nextX, nextY, nextDir)):
			if (self.bot.getStatus()=="CREATED"):
				self.bot.setStatus("PLACED")	

class LeftCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"LEFT", bot, table)

	def doTableCommand(self,args):
		self.bot.setDir((self.bot.getDir()+90) % 360)

class RightCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"RIGHT", bot, table)

	def doTableCommand(self,args):
		self.bot.setDir((self.bot.getDir()-90) % 360)

class ReportCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"REPORT", bot, table)
		self.dirMap={}
		self.dirMap[0]="EAST"
		self.dirMap[90]="NORTH"
		self.dirMap[180]="WEST"
		self.dirMap[270]="SOUTH"

	def doTableCommand(self,args):
		curDir=self.bot.getDir()

		self.setMsg(str(self.bot.getPosX())+","+str(self.bot.getPosY())+","+self.dirMap[curDir])