from botcommands.corecommands import *

class MoveCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"MOVE", bot, table)
		cmdlist=[WestMoveCommand(bot,table),NorthMoveCommand(bot,table), SouthMoveCommand(bot,table)]

		self.__commands={}
		self.__commands[0]=EastMoveCommand(bot,table)
		self.__commands[90]=NorthMoveCommand(bot,table)
		self.__commands[180]=WestMoveCommand(bot,table)
		self.__commands[270]=SouthMoveCommand(bot,table)

	def doTableCommand(self,args):
		curDir=self.bot.getDir()
		self.__commands[curDir].doCommand(args)

class EastMoveCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"MOVE-EAST", bot, table)

	def doTableCommand(self,args):
		curDir=self.bot.getDir()		
		nextX=self.bot.getPosX()+1
		nextY=self.bot.getPosY()		
		self.setBotPosition(nextX, nextY, curDir)

class WestMoveCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"MOVE-WEST", bot, table)

	def doTableCommand(self,args):
		curDir=self.bot.getDir()		
		nextX=self.bot.getPosX()-1
		nextY=self.bot.getPosY()		
		self.setBotPosition(nextX, nextY, curDir)

class NorthMoveCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"MOVE-NORTH", bot, table)

	def doTableCommand(self,args):
		curDir=self.bot.getDir()		
		nextX=self.bot.getPosX()
		nextY=self.bot.getPosY()+1		
		self.setBotPosition(nextX, nextY, curDir)

class SouthMoveCommand(OnTableCommand):

	def __init__(self, bot, table):
		OnTableCommand.__init__(self,"MOVE-SOUTH", bot, table)

	def doCommand(self,args):
		curDir=self.bot.getDir()		
		nextX=self.bot.getPosX()
		nextY=self.bot.getPosY()-1		
		self.setBotPosition(nextX, nextY, curDir)