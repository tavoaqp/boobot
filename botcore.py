class Table:

	def __init__(self, height, width):
		self.height=height
		self.width=width

	def getWidth(self):
		return self.width

	def getHeight(self):
		return self.height


class BooBot:

	def __init__(self):
		self.pos_x=0
		self.pos_y=0
		self.dir=0
		self.status="CREATED"		

	def setPosX(self,x_value):
		self.pos_x=x_value

	def getPosX(self):
		return self.pos_x

	def setPosY(self,y_value):
		self.pos_y=y_value

	def getPosY(self):
		return self.pos_y

	def getStatus(self):
		return self.status

	def setStatus(self,status_value):
		self.status=status_value

	def getDir(self):
		return self.dir

	def setDir(self, dir_value):
		self.dir=dir_value
