import unittest
from botcore import *
from botcommands.dispatcher import *

class TestBooBot(unittest.TestCase):

	def setUp(self):
		self.table=Table(5,5)
		self.bot=BooBot()
		self.dispatcher=BooBotCommmandDispatcher(self.bot, self.table)

	def test_without_place(self):
		self.dispatcher.dispatch("MOVE",[])
		output=self.dispatcher.dispatch("REPORT",[])
		self.assertEqual(output,"")

	def test_place_with_normal_move(self):
		self.dispatcher.dispatch("PLACE",[0,0,"NORTH"])
		self.dispatcher.dispatch("MOVE",[])
		output=self.dispatcher.dispatch("REPORT",[])
		self.assertEqual(output,"0,1,NORTH")

	def test_place_without_move(self):
		self.dispatcher.dispatch("PLACE",[0,0,"NORTH"])
		self.dispatcher.dispatch("LEFT",[])
		output=self.dispatcher.dispatch("REPORT",[])
		self.assertEqual(output,"0,0,WEST")

	def test_place_with_normal_sequence(self):
		self.dispatcher.dispatch("PLACE",[1,2,"EAST"])
		self.dispatcher.dispatch("MOVE",[])
		self.dispatcher.dispatch("MOVE",[])
		self.dispatcher.dispatch("LEFT",[])
		self.dispatcher.dispatch("MOVE",[])
		output=self.dispatcher.dispatch("REPORT",[])
		self.assertEqual(output,"3,3,NORTH")

	def test_place_with_move_outside_box(self):
		self.dispatcher.dispatch("PLACE",[0,0,"SOUTH"])
		self.dispatcher.dispatch("MOVE",[])
		self.dispatcher.dispatch("MOVE",[])
		output=self.dispatcher.dispatch("REPORT",[])
		self.assertEqual(output,"0,0,SOUTH")


if __name__ == '__main__':
    unittest.main()