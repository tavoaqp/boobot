import cmd
from botcore import *
from botcommands.dispatcher import *

class BootBotCmdInterpreter(cmd.Cmd):
    """Boo Bot command line interpreter."""
    
    table=Table(5,5)
    bot=BooBot()
    dispatcher=BooBotCommmandDispatcher(bot, table)

    
    def do_PLACE(self, line):
    	args=line.split(",")
    	if (len(args)!=3):
    		print "Please use format PLACE X,Y,F where X and Y are integer values and F can be NORTH, SOUTH, EAST or WEST"
    	elif (args[2] not in ["NORTH", "SOUTH", "EAST","WEST"]):
    		print "Parameter F can only be NORTH, SOUTH, EAST or WEST"
    	else:    	
	    	x_value=int(args[0])
	    	y_value=int(args[1])
	    	dirVal=args[2]
	    	self.dispatcher.dispatch("PLACE",[x_value,y_value,dirVal])
	    	
    
    def do_MOVE(self, line):
    	self.dispatcher.dispatch("MOVE",[])		

    def do_LEFT(self, line):
		self.dispatcher.dispatch("LEFT",[])		    	

    def do_RIGHT(self, line):
    	self.dispatcher.dispatch("RIGHT",[])		

    def do_REPORT(self, line):
    	output=self.dispatcher.dispatch("REPORT",[])
    	if (len(output)>0):
    		print output

    def do_EOF(self, line):
        return True
		
if __name__ == '__main__':
    BootBotCmdInterpreter().cmdloop()